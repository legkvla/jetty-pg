import junit.framework.Assert;
import org.eclipse.jetty.util.security.Credential;
import org.eclipse.jetty.util.security.Password;
import org.junit.Test;

/**
 * @author legkunec
 * @since 12.03.14 19:09
 */
public class SaltTests {

    @Test
    public void resolvingPassword(){
        final String digest = Credential.MD5.digest("guestguest");
        System.out.println("digest = " + digest);
        Credential md5 = Credential.getCredential(digest);
        Assert.assertTrue(md5.check(new Password("guestguest")));
        Assert.assertTrue(md5.check(Credential.getCredential(digest)));
    }

    @Test
    public void resolvePassword2(){
        //@password="5fd013454f0596ec9e7f0315fc534734"
        final String username="test";
        final String salt="R*>^-ic`x5|";
        final String passwordDigest = Credential.MD5.digest(
                username + Credential.MD5.digest(
                        salt + Credential.MD5.digest("testtest").substring(4)
                ).substring(4)
        ).substring(4);
        System.out.println("pass0digest = " + Credential.MD5.digest(salt + Credential.MD5.digest("testtest").substring(4)).substring(4));
        System.out.println("passwordDigest = " + passwordDigest);
        System.out.println("Credential.MD5.digest(\"testtest\") = " + Credential.MD5.digest("testtest").substring(4));
        Assert.assertEquals("5fd013454f0596ec9e7f0315fc534734", passwordDigest);
    }

//    user_account = $space.userAccount.get "test"
//    puts user_account.check(Digest::MD5.hexdigest('fa1' + '5fd013454f0596ec9e7f0315fc534734'), 'fa1')

}
