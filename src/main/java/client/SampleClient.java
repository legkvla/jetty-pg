package client;


import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

/**
 * @author legkunec
 * @since 11.03.14 19:00
 */
public class SampleClient {

    public static void main(String[] args) throws IOException {
        final DefaultHttpClient httpClient = new DefaultHttpClient();
        httpClient.getCredentialsProvider().setCredentials(
                new AuthScope("localhost", 8090),
                new UsernamePasswordCredentials("guest", "guestguest")
        );

        final HttpGet httpGet = new HttpGet("http://localhost:8081/trains/index");
        httpGet.setHeader("Content-type", "application/json");
        final HttpResponse httpResponse = httpClient.execute(httpGet);
        System.out.println("httpResponse.toString() = " + httpResponse.toString());
        final InputStream inputStream = httpResponse.getEntity().getContent();
        final StringWriter stringWriter = new StringWriter();
        IOUtils.copy(inputStream, stringWriter);
        System.out.println("output = " + stringWriter.toString());
    }
}
