package server;

import org.eclipse.jetty.security.DefaultIdentityService;
import org.eclipse.jetty.security.IdentityService;
import org.eclipse.jetty.security.LoginService;
import org.eclipse.jetty.security.MappedLoginService;
import org.eclipse.jetty.server.UserIdentity;
import org.eclipse.jetty.util.security.Credential;

import javax.security.auth.Subject;
import java.security.Principal;

/**
 * @author legkunec
 * @since 12.03.14 15:34
 */
public class RvecLoginService implements LoginService {

    private IdentityService identityService=new DefaultIdentityService();

    @Override
    public String getName() {
        return "rveck-mock";
    }

    @Override
    public UserIdentity login(String username, Object credentials) {
        final UserIdentity userIdentity = getUserIdentityByLogin();
        if (userIdentity != null) {
            final MappedLoginService.UserPrincipal userPrincipal = (MappedLoginService.UserPrincipal)
                    userIdentity.getUserPrincipal();
            if (userPrincipal.authenticate(credentials)){
                return userIdentity;
            }
        }
        return null;
    }

    private UserIdentity getUserIdentityByLogin() {
        final String defaultLogin = "test";
        final String[] roles = {"user"};

        final Credential credential = new RvecCredential(defaultLogin, "R*>^-ic`x5|",
                "5fd013454f0596ec9e7f0315fc534734");

        final Principal userPrincipal = new MappedLoginService.KnownUser(defaultLogin, credential);

        final Subject subject = new Subject();
        subject.getPrincipals().add(userPrincipal);
        subject.getPrivateCredentials().add(credential);
        subject.setReadOnly();

        return identityService.newUserIdentity(
                subject,
                userPrincipal,
                roles
        );
    }

    @Override
    public boolean validate(UserIdentity user) {
        return true;
    }

    @Override
    public IdentityService getIdentityService() {
        return identityService;
    }

    @Override
    public void setIdentityService(IdentityService service) {
        throw new IllegalStateException("Not implemented");
    }

    @Override
    public void logout(UserIdentity user) {
    }
}
