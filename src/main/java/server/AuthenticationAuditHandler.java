package server;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.server.session.AbstractSession;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationAuditHandler extends AbstractHandler {
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        System.out.println("==========================================================");
        System.out.println("target = " + target);

        String requestedSessionId = request.getRequestedSessionId();
        System.out.println("[AUDIT_HANDLER] requestedSessionId = " + requestedSessionId);
        System.out.println("[AUDIT_HANDLER] isRequestedSessionIdValid = " + request.isRequestedSessionIdValid());

        System.out.println("[AUDIT_HANDLER] request.getRemoteUser() = " + request.getRemoteUser());
        System.out.println("[AUDIT_HANDLER] request.getRemoteAddr() = " + request.getRemoteAddr());
//        System.out.println("[AUDIT_HANDLER] request.getRemoteHost() = " + request.getRemoteHost());

        final HttpSession httpSession = baseRequest.getSession(false);

        if (httpSession == null) return;
        //sessionManager.getHttpSession(request.getRequestedSessionId());

        final Boolean sessionAuth = (Boolean) httpSession.
                getAttribute(AbstractSession.SESSION_KNOWN_ONLY_TO_AUTHENTICATED);
        System.out.println("[AUDIT_HANDLER] sessionAuth : " + sessionAuth);
        System.out.println("[AUDIT_HANDLER] sessionId : " + httpSession.getId());
        System.out.println("[AUDIT_HANDLER] session.isNew : " + httpSession.isNew());

        if (httpSession instanceof AbstractSession) {
            AbstractSession session = (AbstractSession) httpSession;
            System.out.println("[AUDIT_HANDLER] httpSession id changed : " + session.isIdChanged());
        }
    }
}

