package server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class UploadImageServer {
    public static void main(String[] args) throws Exception {
        final Server server = new Server(8090);
        final HandlerCollection handlerList = new HandlerCollection();
        final ServletContextHandler context = new ServletContextHandler(handlerList, "/");
        context.addServlet(new ServletHolder(new UploadImageServlet()), "/upload_image");
        server.setHandler(handlerList);
        server.start();
        server.join();
    }
}
