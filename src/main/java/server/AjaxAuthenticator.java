package server;

import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.HttpHeaderValue;
import org.eclipse.jetty.security.ServerAuthException;
import org.eclipse.jetty.security.authentication.DeferredAuthentication;
import org.eclipse.jetty.security.authentication.FormAuthenticator;
import org.eclipse.jetty.security.authentication.SessionAuthentication;
import org.eclipse.jetty.server.Authentication;
import org.eclipse.jetty.server.UserIdentity;
import org.eclipse.jetty.util.URIUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author legkunec
 * @since 08.04.14 15:19
 */
public class AjaxAuthenticator extends FormAuthenticator {

    private final String loginPage;

    public AjaxAuthenticator(String login, String error, boolean dispatch) {
        super(login, error, dispatch);
        loginPage = login;
    }

    @Override
    public Authentication validateRequest(ServletRequest req, ServletResponse res, boolean mandatory) throws ServerAuthException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        String uri = request.getRequestURI();
        if (uri==null)
            uri= URIUtil.SLASH;

        mandatory |= this.isJSecurityCheck(uri);
        if (!mandatory)
            return new DeferredAuthentication(this);

        if (this.isLoginOrErrorPage(URIUtil.addPaths(request.getServletPath(), request.getPathInfo())) &&!DeferredAuthentication.isDeferred(response))
            return new DeferredAuthentication(this);

        HttpSession session = request.getSession(true);

        try
        {
            // Handle a request for authentication.
            if (this.isJSecurityCheck(uri))
            {
                final String username = request.getParameter(__J_USERNAME);
                final String password = request.getParameter(__J_PASSWORD);

                UserIdentity user = this.login(username, password, request);
                session = request.getSession(true);
                if (user!=null)
                {
                    FormAuthentication form_auth;
                    synchronized(session)
                    {
                        form_auth = new FormAuthentication(this.getAuthMethod(),user);
                    }

                    response.getOutputStream().println("<html><head><title>Authentication was succesful</title></head><body>Authentication was succesful</body></html>");
                    response.setStatus(HttpServletResponse.SC_OK);
                    return form_auth;
                }

                // not authenticated
                if (response != null) response.sendError(HttpServletResponse.SC_FORBIDDEN);
                return Authentication.SEND_FAILURE;
            }

            // Look for cached authentication
            Authentication authentication = (Authentication) session.getAttribute(SessionAuthentication.__J_AUTHENTICATED);
            if (authentication != null)
            {
                // Has authentication been revoked?
                if (authentication instanceof Authentication.User &&
                    _loginService!=null &&
                    !_loginService.validate(((Authentication.User)authentication).getUserIdentity()))
                {
                    session.removeAttribute(SessionAuthentication.__J_AUTHENTICATED);
                }
                else
                {
                    return authentication;
                }
            }

            // if we can't send challenge
            if (DeferredAuthentication.isDeferred(response))
            {
                return Authentication.UNAUTHENTICATED;
            }

            // send the the challenge
            RequestDispatcher dispatcher = request.getRequestDispatcher(loginPage);
            response.setHeader(HttpHeader.CACHE_CONTROL.asString(), HttpHeaderValue.NO_CACHE.asString());
            response.setDateHeader(HttpHeader.EXPIRES.asString(), 1);
            dispatcher.forward(new FormRequest(request), new FormResponse(response));
            return Authentication.SEND_CONTINUE;
        }
        catch (Exception e)
        {
            throw new ServerAuthException(e);
        }
    }
}
