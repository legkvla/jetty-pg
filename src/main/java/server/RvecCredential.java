package server;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.eclipse.jetty.util.security.Credential;
import org.eclipse.jetty.util.security.Password;

/**
 * @author legkunec
 * @since 13.03.14 13:01
 */
public class RvecCredential extends Credential {

    private static final Logger LOG = Log.getLogger(RvecCredential.class);

    private static final long serialVersionUID = 1;

    private final String login;
    private final String salt;
    private final String passwordDigest;

    public RvecCredential(String login, String salt, String passwordDigest) {
        this.login = login;
        this.salt = salt;
        this.passwordDigest = passwordDigest;
    }

    @Override
    public boolean check(Object credentials) {
        if (this == credentials) return true;

        if (credentials instanceof Password || credentials instanceof String){
            final String password = credentials.toString();
            if (passwordDigest.equals(generateDigest(password))) return true;
        }
        return false;
    }

    String generateDigest(String password){
        return Credential.MD5.digest(
                login + Credential.MD5.digest(
                        salt + Credential.MD5.digest(password).substring(4)
                ).substring(4)
        ).substring(4);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RvecCredential that = (RvecCredential) o;

        if (!passwordDigest.equals(that.passwordDigest)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return passwordDigest.hashCode();
    }

    @Override
    public String toString() {
        return "RvecCredential{" +
                "login='" + login + '\'' +
                '}';
    }
}
