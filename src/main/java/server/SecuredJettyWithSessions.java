package server;

import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.server.session.HashSessionIdManager;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.security.Password;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.EnumSet;

/**
 * @author legkunec
 * @since 13.03.14 17:24
 */
public class SecuredJettyWithSessions {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8090);
        HandlerCollection handlerList = new HandlerCollection();
        handlerList.addHandler(createResourceHandler());

        ServletContextHandler context = new ServletContextHandler(handlerList, "/", ServletContextHandler.SESSIONS | ServletContextHandler.SECURITY);

        //session timeout
        context.getSessionHandler().getSessionManager().setMaxInactiveInterval(3000);

        context.addFilter(new FilterHolder(new Filter() {
            @Override
            public void init(FilterConfig filterConfig) throws ServletException {
            }

            @Override
            public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

//                if (request instanceof HttpServletRequest) {
//                    HttpServletRequest httpServletRequest = (HttpServletRequest) request;
//                    HttpSession session = httpServletRequest.getSession();
//                    final Boolean sessionAuth = (Boolean) session.getAttribute(AbstractSession.SESSION_KNOWN_ONLY_TO_AUTHENTICATED);
//                    System.out.println("sessionAuth = " + sessionAuth);
//                }

                chain.doFilter(request, response);

//                final String requestAsString = IOUtils.toString(request.getInputStream(),
//                        request.getCharacterEncoding());
//                System.out.println("requestAsString = " + requestAsString);

//                final HttpServletResponseCopier responseWrapper = new HttpServletResponseCopier((HttpServletResponse) response);
//                try {
//                    chain.doFilter(request, responseWrapper);
//                    responseWrapper.flushBuffer();
//                } finally {
//                    byte[] copy = responseWrapper.getCopy();
//                    System.out.println(new String(copy, response.getCharacterEncoding()));
//                    System.out.println("((HttpServletResponse) response).getStatus() = " + ((HttpServletResponse) response).getStatus());
//                    System.out.println("((HttpServletResponse) response).getContentType() = " + response.getContentType());
//                }
            }

            @Override
            public void destroy() {
            }
        }), "/*", EnumSet.of(DispatcherType.REQUEST));

        context.addServlet(new ServletHolder(new DefaultServlet() {
            @Override
            protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                response.setContentType("application/json-rpc; charset=UTF-8");
                response.getWriter().append("{\"message\":\"Hello, friend!\"}");
                //append(request.getUserPrincipal().getName());
            }

            @Override
            protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                doGet(request, response);
            }
        }), "/servlet");

        context.addServlet(new ServletHolder(new DefaultServlet() {
            @Override
            protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                /*response.getWriter().append("<form method='POST' action='/j_security_check'>"
                        + "<input type='text' name='j_username'/>"
                        + "<input type='password' name='j_password'/>"
                        + "<input type='submit' value='Login'/></form>");*/
                System.out.println("request.getContentType() = " + request.getContentType());
                response.setContentType("text/html; charset=UTF-8");
                response.setStatus(401);
                //<meta charset="UTF-8">
                response.getWriter().append("Не авторизован");
            }
        }), "/login");

        context.addServlet(new ServletHolder(new DefaultServlet() {
            @Override
            protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                request.getSession().invalidate();
                System.out.println("request.getUserPrincipal().getName() = " + request.getRemoteUser());
                response.getWriter().append("<html><body>You have been logged out (session ivalidate)</body></html>");
            }
        }), "/logout");

        context.addServlet(new ServletHolder(new DefaultServlet() {
            @Override
            protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                //so looks like logout method doesn't work for FormAuthenticator.
                // And right way only is to invalidate session.
                request.logout();
                response.getWriter().append("<html><body>You have been logged out (true logout)</body></html>");
            }
        }), "/logout2");

        context.addServlet(new ServletHolder(new DefaultServlet() {
            @Override
            protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                response.getWriter().append("<html><body>Hello world!</body></html>");
            }
        }), "/hello");

        Constraint constraint = new Constraint();
        constraint.setName(Constraint.__FORM_AUTH);
        constraint.setRoles(new String[]{"user", "admin", "moderator"});
        constraint.setAuthenticate(true);

        ConstraintMapping constraintMapping = new ConstraintMapping();
        constraintMapping.setConstraint(constraint);
        constraintMapping.setPathSpec("/hello/*");

        ConstraintSecurityHandler securityHandler = new ConstraintSecurityHandler();
        securityHandler.addConstraintMapping(constraintMapping);
        HashLoginService loginService = new HashLoginService();
        loginService.putUser("username", new Password("password"), new String[]{"user"});
        securityHandler.setLoginService(loginService);

        AjaxAuthenticator authenticator = new AjaxAuthenticator("/login", "/login", true);
        securityHandler.setAuthenticator(authenticator);

        context.setSecurityHandler(securityHandler);

//        handlerList.addHandler(new HelloHandler("Last handler", "Last handler"));
        handlerList.addHandler(new AuthenticationAuditHandler());
        handlerList.addHandler(createRequestLogHandler());

        server.setHandler(handlerList);

        final HashSessionIdManager sessionIdManager = new HashSessionIdManager();
        sessionIdManager.setWorkerName("worker1");

        server.setSessionIdManager(sessionIdManager);
        server.start();
        server.join();
    }

    private static ResourceHandler createResourceHandler() {
        final ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setDirectoriesListed(false);
        resourceHandler.setWelcomeFiles(new String[]{"index.html"});
        resourceHandler.setResourceBase("public");
        return resourceHandler;
    }

    private static RequestLogHandler createRequestLogHandler() {
        RequestLogHandler requestLogHandler = new RequestLogHandler();

        NCSARequestLog requestLog = new NCSARequestLog("./logs/jetty-yyyy_mm_dd.request.log");
        requestLog.setRetainDays(90);
        requestLog.setAppend(true);
        requestLog.setExtended(false);
        requestLog.setLogTimeZone("GMT");
        requestLogHandler.setRequestLog(requestLog);
        return requestLogHandler;
    }
}
