package server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.File;

/**
 * @author legkunec
 * @since 24.04.14 10:20
 */
public class EmbeddedWebAppContainer {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8081);

        WebAppContext webapp = new WebAppContext();
        webapp.setDisplayName("ebay");
        webapp.setExtractWAR(false);
        webapp.setContextPath("/");
        webapp.setWar("webapps/async-rest.war");
        webapp.setTempDirectory(new File("webapps/temp"));
        server.setHandler(webapp);

        server.start();
        server.join();
    }
}
