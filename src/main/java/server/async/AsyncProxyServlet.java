package server.async;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AsyncProxyServlet extends HttpServlet{

    public static final String DELEGATED_WORKER_ATTRIBUTE = "delegatedWorker";

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpWorker delegatedServlet = (HttpWorker) request.getServletContext().getAttribute(DELEGATED_WORKER_ATTRIBUTE);

        final AsyncContext asyncContext = request.startAsync(request, response);
        asyncContext.setTimeout(0);
        final ServletJob servletJob = new ServletJob(asyncContext, delegatedServlet);
        request.getInputStream().setReadListener(servletJob);
        response.getOutputStream().setWriteListener(servletJob);
    }
}
