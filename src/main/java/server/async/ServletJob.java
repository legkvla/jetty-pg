package server.async;

import javax.servlet.AsyncContext;
import javax.servlet.ReadListener;
import javax.servlet.WriteListener;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServletJob implements ReadListener, WriteListener, Runnable {

    private final static Logger LOG = Logger.getLogger(ServletJob.class.getName());
    private static final int N_THREADS = 4;

    private final AsyncContext asyncContext;
    private final ServletState servletState = new ServletState();

    private final HttpWorker delegatedServlet;

    private final Executor executor;

    private final AtomicInteger threadCounter = new AtomicInteger(0);

    public ServletJob(AsyncContext asyncContext, HttpWorker delegatedServlet) {
        this.asyncContext = asyncContext;
        this.delegatedServlet = delegatedServlet;
        executor = Executors.newFixedThreadPool(N_THREADS, new ThreadFactory() {
            public Thread newThread(Runnable r) {
                return new Thread(r, "ProxyThread-" + threadCounter.getAndIncrement());
            }
        });
    }

    public void onDataAvailable() throws IOException {

    }

    public void onAllDataRead() throws IOException {
        servletState.canRead();
    }

    public void onWritePossible() throws IOException {
        servletState.canWrite();
    }

    public void onError(Throwable t) {
        LOG.log(Level.SEVERE, "Error while processing request", t);
        asyncContext.complete();
    }

    public void run() {
        try {
            delegatedServlet.service(asyncContext.getRequest(), asyncContext.getResponse());
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Error while processing request", e);
        }
        asyncContext.complete();
    }

    private class ServletState{
        private boolean canRead = false;
        private boolean canWrite = false;

        synchronized void canRead(){
            canRead = true;
            trigger();
        }

        synchronized void canWrite(){
            canWrite = true;
            trigger();
        }

        private void trigger(){
            if (canRead && canWrite) executor.execute(ServletJob.this);
        }
    }
}
