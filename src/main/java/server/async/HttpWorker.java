package server.async;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

public interface HttpWorker {
    void service(ServletRequest request, ServletResponse response) throws ServletException, IOException;
}
