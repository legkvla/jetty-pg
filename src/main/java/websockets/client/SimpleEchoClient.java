package websockets.client;

import java.net.HttpCookie;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.HttpCookieStore;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;

/**
 * Example of a simple Echo Client.
 */
public class SimpleEchoClient {

    public static void main(String[] args) throws InterruptedException, URISyntaxException {
        String destUri = "ws://localhost:8081/trains";
        if (args.length > 0) {
            destUri = args[0];
        }
        WebSocketClient client = new WebSocketClient();
//        HttpCookieStore cookieStore = new HttpCookieStore();
//        cookieStore.add(new URI("http://localhost:8080/"), new HttpCookie("JSESSIONID", "2y37ewu44km717epsd4rw1u95"));
//        client.setCookieStore(cookieStore);
        SimpleEchoSocket socket = new SimpleEchoSocket();
        try {
            client.start();
            URI echoUri = new URI(destUri);
            ClientUpgradeRequest request = new ClientUpgradeRequest();
            client.connect(socket, echoUri, request);
            System.out.printf("Connecting to : %s%n", echoUri);
            socket.awaitClose(1000, TimeUnit.SECONDS);
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            try {
                client.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Thread.sleep(200000);
    }
}
