package websockets.server;


import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.SecurityHandler;
import org.eclipse.jetty.security.authentication.FormAuthenticator;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.security.Password;
import org.eclipse.jetty.websocket.jsr356.server.ServerContainer;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpointConfig;
import java.io.IOException;

/**
 * @author legkunec
 * @since 02.04.14 15:46
 */
public class EchoEndpoint extends Endpoint {

    public static final String RESPONSE_MSG = "ResponseMsg";

    @Override
    public void onOpen(final Session session, final EndpointConfig endpointConfig) {
        session.addMessageHandler(new MessageHandler.Whole<String>() {
            @Override
            public void onMessage(String msg) {
                System.out.println("msg = " + msg);
                String text = (String) endpointConfig.getUserProperties().get(RESPONSE_MSG);
                System.out.println("response = " + text);
                try {
                    session.getBasicRemote().sendText("{\"type\":\"message\",\"data\":\"" + text + "\"}");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void main(String[] args) throws Exception
    {
        Server server = new Server(8080);

        HandlerList handlerList = new HandlerList();

        handlerList.addHandler(createResourceHandler());
        ServletContextHandler context = new ServletContextHandler(handlerList, "/", ServletContextHandler.SESSIONS | ServletContextHandler.SECURITY);
        handlerList.addHandler(createRequestLogHandler());
        server.setHandler(handlerList);

        createLoginServlet(context);
        context.setSecurityHandler(createSecurityHandler());

        // Enable javax.websocket configuration for the context
        ServerContainer wsContainer = WebSocketServerContainerInitializer.configureContext(context);
        ServerEndpointConfig.Builder builder = ServerEndpointConfig.Builder.create(EchoEndpoint.class, "/echo");

        ServerEndpointConfig config = builder.build();
        config.getUserProperties().put(RESPONSE_MSG, "Hello!");
        wsContainer.addEndpoint(config);

        server.start();
        context.dumpStdErr();
        server.join();
    }

    private static void createLoginServlet(ServletContextHandler context) {
        context.addServlet(new ServletHolder(new DefaultServlet() {
            @Override
            protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                /*response.getWriter().append("<form method='POST' action='/j_security_check'>"
                        + "<input type='text' name='j_username'/>"
                        + "<input type='password' name='j_password'/>"
                        + "<input type='submit' value='Login'/></form>");*/
                System.out.println("request.getContentType() = " + request.getContentType());
                response.setContentType("text/html; charset=UTF-8");
                response.setStatus(401);
                //<meta charset="UTF-8">
                response.getWriter().append("Not authorised");
            }
        }), "/login");
    }

    private static SecurityHandler createSecurityHandler() {

        Constraint constraint = new Constraint();
        constraint.setName(Constraint.__FORM_AUTH);
        constraint.setRoles(new String[]{"user","admin","moderator"});
        constraint.setAuthenticate(true);

        ConstraintMapping constraintMapping = new ConstraintMapping();
        constraintMapping.setConstraint(constraint);
        constraintMapping.setPathSpec("/*");

        ConstraintSecurityHandler securityHandler = new ConstraintSecurityHandler();
        securityHandler.addConstraintMapping(constraintMapping);
        HashLoginService loginService = new HashLoginService();
        loginService.putUser("username", new Password("password"), new String[] {"user"});
        securityHandler.setLoginService(loginService);

        FormAuthenticator authenticator = new FormAuthenticator("/login", "/login", false);
        securityHandler.setAuthenticator(authenticator);

        return securityHandler;
    }

    private static ResourceHandler createResourceHandler() {
        final ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setDirectoriesListed(false);
        resourceHandler.setWelcomeFiles(new String[]{"index.html"});
        resourceHandler.setResourceBase("public");
        return resourceHandler;
    }

    private static RequestLogHandler createRequestLogHandler() {
        RequestLogHandler requestLogHandler = new RequestLogHandler();

        NCSARequestLog requestLog = new NCSARequestLog("./logs/jetty-yyyy_mm_dd.request.log");
        requestLog.setRetainDays(90);
        requestLog.setAppend(true);
        requestLog.setExtended(false);
        requestLog.setLogTimeZone("GMT");
        requestLogHandler.setRequestLog(requestLog);
        return requestLogHandler;
    }
}
